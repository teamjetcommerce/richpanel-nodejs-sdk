const Validator = require('jsonschema').Validator;

class validation {
  order(eventData) {
    let v = new Validator();
    let schema = {
      "$id": "orderSchema",
      "type": "object",
      "required": [
        "event",
        "properties",
        "userProperties"
      ],
      "properties": {
        "event": {
          "$id": "#/properties/event",
          "type": "string"
        },
        "properties": {
          "$id": "#/properties/properties",
          "type": "object",
          "required": [
            "orderId"
          ],
          "properties": {
            "orderId": {
              "$id": "#/properties/properties/properties/orderId",
              "type": "string"
            }
          }
        },
        "userProperties": {
          "$id": "#/properties/userProperties",
          "type": "object",
          "required": [
            "uid"
          ],
          "properties": {
            "uid": {
              "$id": "#/properties/userProperties/properties/uid",
              "type": "string"
            }
          }
        }
      }
    };

    return v.validate(eventData, schema)
  }

  identify(eventData) {
    let v = new Validator();
    let schema = {
      "$id": "customSchema",
      "type": "object",
      "required": [
        "event",
        "userProperties"
      ],
      "properties": {
        "event": {
          "$id": "#/properties/event",
          "type": "string"
        },
        "userProperties": {
          "$id": "#/properties/userProperties",
          "type": "object",
          "required": [
            "uid"
          ],
          "properties": {
            "uid": {
              "$id": "#/properties/userProperties/properties/uid",
              "type": "string"
            }
          }
        }
      }
    };

    return v.validate(eventData, schema)
  }

  customEvent(eventData) {
    let v = new Validator();
    let schema = {
      "$id": "identifySchema",
      "type": "object",
      "required": [
        "event",
        "properties",
        "userProperties"
      ],
      "properties": {
        "event": {
          "$id": "#/properties/event",
          "type": "string"
        },
        "userProperties": {
          "$id": "#/properties/userProperties",
          "type": "object",
          "required": [
            "uid"
          ],
          "properties": {
            "uid": {
              "$id": "#/properties/userProperties/properties/uid",
              "type": "string"
            }
          }
        }
      }
    };

    return v.validate(eventData, schema)
  }
}

module.exports = validation