const request = require('request')
const validation = require('./validation')

class richpanel {

  /**
   * constructor for richpanel
   * @param {Object} options 
   * @param {String} options.appClientId appclient id of connectoer
   * @param {String} options.connectorSecret connector secret of connecter
   * @param {String} [options.env='production'] environment. ex: development, production
   */
  constructor(options) {
    this.appClientId = options.appClientId
    this.connectorSecret = options.connectorSecret
    this.env = options.env || 'production'
    this.version = '2.0.0'
    if (options.env === 'development') {
      this.apiUrl = 'https://api-dev.richpanel.com/v1/t'
      this.apiBatchUrl = 'https://api-dev.richpanel.com/v1/t'
    } else {
      /* need to change */
      this.apiUrl = 'https://api.richpanel.com/v1/t'
      this.apiBatchUrl = 'https://api.richpanel.com/v1/t'
    }
  }

  /**
   * Tracking the events of richpanel
   * @param {String} type type of event. ex: contact, order etc,.
   * @param {Object} properties extra properties
   * @param {Object} userProperties user properties
   * @param {Number} originalTimestamp original event timestamp
   * @param {Object} [context] browser details about user who places the order
   */
  async track(event, properties, userProperties, originalTimestamp, context) {
    return new Promise((resolve, reject) => {
      let sendObject = {}
      let currentDateInTimestamp = Date.now()

      sendObject = {
        event: event,
        properties: properties,
        userProperties: userProperties,
        time: {
          sentAt: currentDateInTimestamp
        },
        appClientId: this.appClientId,
        version: this.version,
        did: null,
        sid: null
      }


      if (originalTimestamp !== null && originalTimestamp !== undefined) {
        sendObject.time.originalTimestamp = originalTimestamp
      }

      if (context !== null && context !== undefined && context !== {}) {
        sendObject.context = context
      }

      /* validate the data */
      let validationObject = new validation()
      let validationData
      if (event === 'order') {
        validationData = validationObject.order(sendObject)
      } else if (event === 'identify') {
        validationData = validationObject.identify(sendObject)
      } else {
        validationData = validationObject.customEvent(sendObject)
      }

      let validationError = []
      /* checking any errors is present */
      if (validationData.errors.length !== 0) {
        /* adding all error messages into array */
        validationData.errors.forEach((element) => {
          validationError.push(element.message)
        })

        reject({
          error: true,
          message: 'Error while processing data',
          data: validationError
        })
      } else {

        console.log('sending in track event ', sendObject)

        let body = {
          h: this.formatData(sendObject)
        };

        /* send data to kinesis */
        request({
          url: this.apiUrl,
          method: "POST",
          json: true,
          body: body
        }, function (error, response, body) {
          console.log('in request', error, body)
          if (error) {
            reject({
              error: true,
              message: error
            })
          } else {
            resolve({
              error: false,
              message: 'successfully sent data'
            })
          }
        });
      }
    })
  }

  /**
   * Sending batch the events of richpanel
   * @param {Array} events events array
   * @param {Object} events.event event type
   * @param {Object} events.properties properties of event
   * @param {Object} events.userProperties user properties regarding event
   * @param {Object} events.originalTimestamp original time stamp regarding event
   */
  batchSend(events) {
    return new Promise((resolve, reject) => {
      let sendObject = {
        event: 'send_batch',
        appClientId: this.appClientId,
      }
      let sendObjectsArray = []
      let currentDateInTimestamp = Date.now()
      
      events.forEach(element => {
        let tempEvent = element
        tempEvent.time = {
          sentAt: currentDateInTimestamp
        }
        tempEvent.appClientId = this.appClientId
        tempEvent.version = this.version
        tempEvent.did = null
        tempEvent.sid = null

        if (element.originalTimestamp !== null && element.originalTimestamp !== undefined) {
          tempEvent.time.originalTimestamp = element.originalTimestamp
        }

        delete tempEvent.originalTimestamp
        if (tempEvent.event === 'identify') {
          delete tempEvent.properties
        }

        /* adding into sendObjectsArray */
        sendObjectsArray.push(tempEvent)
      });

      sendObject.events = sendObjectsArray

      /* validation */
      let validationObject = new validation()
      let validationError = []

      /* validate the data */
      /* displaying only one event error message */
      let isErrorPresent = false
      sendObject.events.forEach((element) => {
        let tempValidationData
        if (element.event === 'order') {
          tempValidationData = validationObject.order(element)
        } else if (element.event === 'identify') {
          tempValidationData = validationObject.identify(element)
        } else {
          tempValidationData = validationObject.customEvent(element)
        }

        let tempValidationError = []

        /* checking any errors is present */
        if (tempValidationData.errors.length !== 0) {
          isErrorPresent = true
          /* adding all error messages into array */
          tempValidationData.errors.forEach((element) => {
            tempValidationError.push(element.message)
          })

          validationError.push(tempValidationError)
        } else {
          validationError.push(null)
        }

      })

      /* checking any errors is present */
      if (isErrorPresent) {
        reject({
          error: true,
          message: 'Error while processing data',
          data: validationError
        })
      } else {
        console.log('sending object ', JSON.stringify(sendObject, null, 2))
        let body = {
          h: this.formatData(sendObject)
        }

        /* send data to kinesis */
        request({
          url: this.apiBatchUrl,
          method: "POST",
          json: true,
          body: body
        }, function (error, response, body) {
          console.log('response from kinesis -> ', error, body)
          if (error) {
            reject({
              error: true,
              message: error
            })
          } else {
            resolve({
              error: false,
              message: 'successfully sent data'
            })
          }
        });
      }
    });
  }

  /**
  * conversion jsonData to base64
  * @param {Object} jsonData json data
  */
  formatData(jsonData) {
    let base64Data = Buffer.from(JSON.stringify(jsonData)).toString('base64')
    return base64Data
  }

}

module.exports = richpanel