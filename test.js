const richpanel = require('./index')

async function ss() {
    /* testing package */
    let r = new richpanel({
        appClientId: 'fakeClientId',
        connectorSecret: 'fakeSecret',
        env: 'development'
    })

    try {
        // let data = await r.track("order", {
        //     "orderId": "10717",
        //     "orderStatus": "pending",
        //     "amount": 101.9,
        //     "shippingAmount": 12,
        //     "taxAmount": 0,
        //     "discountAmount": 0,
        //     "items": [
        //         {
        //             "id": "182",
        //             "price": "44.95",
        //             "url": "http://omega3innovations.swiftpim.in/omega-cure/",
        //             "sku": "1_OC", // variant id
        //             "name": "Omega Cure",
        //             "image_url": [
        //                 "http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/16013643/omega-cure1.png",
        //                 "http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/16013643/omega-cure1.png",
        //                 "http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/16013643/omega-cure1.png",
        //                 "http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/16013643/omega-cure1.png"
        //             ],
        //             "categories": [
        //                 {
        //                     "id": "232",
        //                     "name": "Omega Cure",
        //                     "parent": [
        //                         {
        //                             "id": "252",
        //                             "name": "Products"
        //                         }
        //                     ]
        //                 }
        //             ],
        //             "quantity": "2"
        //         }
        //     ],
        //     "shipping_method": "Standard Shipping (2-3 days)",
        //     "payment_method": "Credit Card OLD",
        //     "email": "shubhanshu01@richpanel.com",
        //     "first_name": "Shubhanshu",
        //     "last_name": "Chouhan",
        //     "billing_phone": "9977090117",
        //     "billing_city": "blah",
        //     "billing_region": "CA",
        //     "billing_postcode": "12345",
        //     "billing_country": "US",
        //     "billing_address_line_1": "blah blah",
        //     "context": "new"
        // }, {
        //         "uid": "shubhanshu01@richpanel.com",
        //         "email": "shubhanshu01@richpanel.com",
        //         "name": "shubhanshu01",
        //         "firstName": "Shubhanshu",
        //         "lastName": "Sharma",
        //         "lastLogin": "2018-10-15 13:13:04",
        //         "facebook": "",
        //         "twitter": "",
        //         "linkedin": "",
        //         "instagram": "",
        //         "pinterest": "",
        //         "tumblr": "",
        //         "googleplus": "",
        //         "billingAddress": {
        //             "firstName": "Shubhanshu",
        //             "lastName": "Chouhan",
        //             "city": "blah",
        //             "state": "CA",
        //             "country": "US",
        //             "email": "shubhanshu01@richpanel.com",
        //             "postcode": "12345",
        //             "phone": "9977090117",
        //             "address1": "blah blah",
        //             "address2": ""
        //         },
        //         "shippingAddress": {
        //             "firstName": "Shubhanshu",
        //             "lastName": "Chouhan",
        //             "city": "blah",
        //             "state": "CA",
        //             "country": "US",
        //             "email": "",
        //             "postcode": "12345",
        //             "phone": "",
        //             "address1": "blah blah",
        //             "address2": ""
        //         },
        //         "sourceId": 631
        //     })

        // console.log('data ', data)

        // let data2 = await r.track("identify", {}, {
        //     "uid": "shubhanshu01@richpanel.com",
        //     "email": "shubhanshu01@richpanel.com",
        //     "name": "shubhanshu01",
        //     "firstName": "Shubhanshu",
        //     "lastName": "Sharma",
        //     "lastLogin": "2018-10-15 13:13:04",
        //     "facebook": "",
        //     "twitter": "",
        //     "linkedin": "",
        //     "instagram": "",
        //     "pinterest": "",
        //     "tumblr": "",
        //     "googleplus": "",
        //     "billingAddress": {
        //         "firstName": "Shubhanshu",
        //         "lastName": "Chouhan",
        //         "city": "blah",
        //         "state": "AK",
        //         "country": "US",
        //         "email": "shubhanshu01@richpanel.com",
        //         "postcode": "12345",
        //         "phone": "9977090117",
        //         "address1": "blah blah",
        //         "address2": ""
        //     },
        //     "shippingAddress": {
        //         "firstName": "Shubhanshu",
        //         "lastName": "Chouhan",
        //         "city": "blah",
        //         "state": "AK",
        //         "country": "US",
        //         "email": "",
        //         "postcode": "12345",
        //         "phone": "",
        //         "address1": "blah blah",
        //         "address2": ""
        //     }
        // }, Date.now())

        // console.log('data2 ', data2)

        let data3 = await r.batchSend([
            {
                event: 'order',
                properties: {
                    "orderId": "10717",
                    "orderStatus": "pending",
                    "amount": 101.9,
                    "shippingAmount": 12,
                    "taxAmount": 0,
                    "discountAmount": 0,
                    "items": [
                        {
                            "id": "182",
                            "price": "44.95",
                            "url": "http://omega3innovations.swiftpim.in/omega-cure/",
                            "sku": "1_OC", // variant id
                            "name": "Omega Cure",
                            "image_url": [
                                "http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/16013643/omega-cure1.png",
                                "http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/16013643/omega-cure1.png",
                                "http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/16013643/omega-cure1.png",
                                "http://cdn.swiftpim.in/wp-content/uploads/sites/10/2016/10/16013643/omega-cure1.png"
                            ],
                            "categories": [
                                {
                                    "id": "232",
                                    "name": "Omega Cure",
                                    "parent": [
                                        {
                                            "id": "252",
                                            "name": "Products"
                                        }
                                    ]
                                }
                            ],
                            "quantity": "2"
                        }
                    ],
                    "shipping_method": "Standard Shipping (2-3 days)",
                    "payment_method": "Credit Card OLD",
                    "email": "shubhanshu01@richpanel.com",
                    "first_name": "Shubhanshu",
                    "last_name": "Chouhan",
                    "billing_phone": "9977090117",
                    "billing_city": "blah",
                    "billing_region": "CA",
                    "billing_postcode": "12345",
                    "billing_country": "US",
                    "billing_address_line_1": "blah blah",
                    "context": "new"
                },
                userProperties: {
                    "uid": "shubhanshu01@richpanel.com",
                    "email": "shubhanshu01@richpanel.com",
                    "name": "shubhanshu01",
                    "firstName": "Shubhanshu",
                    "lastName": "Sharma",
                    "lastLogin": "2018-10-15 13:13:04",
                    "facebook": "",
                    "twitter": "",
                    "linkedin": "",
                    "instagram": "",
                    "pinterest": "",
                    "tumblr": "",
                    "googleplus": "",
                    "billingAddress": {
                        "firstName": "Shubhanshu",
                        "lastName": "Chouhan",
                        "city": "blah",
                        "state": "CA",
                        "country": "US",
                        "email": "shubhanshu01@richpanel.com",
                        "postcode": "12345",
                        "phone": "9977090117",
                        "address1": "blah blah",
                        "address2": ""
                    },
                    "shippingAddress": {
                        "firstName": "Shubhanshu",
                        "lastName": "Chouhan",
                        "city": "blah",
                        "state": "CA",
                        "country": "US",
                        "email": "",
                        "postcode": "12345",
                        "phone": "",
                        "address1": "blah blah",
                        "address2": ""
                    },
                    "sourceId": 631
                }
            }
            ,{
                event: 'identify',
                properties: {},
                userProperties: {
                    "uid": "shubhanshu01@richpanel.com",
                    "email": "shubhanshu01@richpanel.com",
                    "name": "shubhanshu01",
                    "firstName": "Shubhanshu",
                    "lastName": "Sharma",
                    "lastLogin": "2018-10-15 13:13:04",
                    "facebook": "",
                    "twitter": "",
                    "linkedin": "",
                    "instagram": "",
                    "pinterest": "",
                    "tumblr": "",
                    "googleplus": "",
                    "billingAddress": {
                        "firstName": "Shubhanshu",
                        "lastName": "Chouhan",
                        "city": "blah",
                        "state": "AK",
                        "country": "US",
                        "email": "shubhanshu01@richpanel.com",
                        "postcode": "12345",
                        "phone": "9977090117",
                        "address1": "blah blah",
                        "address2": ""
                    },
                    "shippingAddress": {
                        "firstName": "Shubhanshu",
                        "lastName": "Chouhan",
                        "city": "blah",
                        "state": "AK",
                        "country": "US",
                        "email": "",
                        "postcode": "12345",
                        "phone": "",
                        "address1": "blah blah",
                        "address2": ""
                    }
                },
                originalTimestamp: Date.now()
            }
            ,{
                event: 'cart_new',
                properties: {
                    
                },
                userProperties: {
                    "uid": "shubhanshu01@richpanel.com",
                    "email": "shubhanshu01@richpanel.com",
                    "name": "shubhanshu01",
                    "firstName": "Shubhanshu",
                    "lastName": "Sharma",
                    "lastLogin": "2018-10-15 13:13:04",
                    "facebook": "",
                    "twitter": "",
                    "linkedin": "",
                    "instagram": "",
                    "pinterest": "",
                    "tumblr": "",
                    "googleplus": "",
                    "billingAddress": {
                        "firstName": "Shubhanshu",
                        "lastName": "Chouhan",
                        "city": "blah",
                        "state": "AK",
                        "country": "US",
                        "email": "shubhanshu01@richpanel.com",
                        "postcode": "12345",
                        "phone": "9977090117",
                        "address1": "blah blah",
                        "address2": ""
                    },
                    "shippingAddress": {
                        "firstName": "Shubhanshu",
                        "lastName": "Chouhan",
                        "city": "blah",
                        "state": "AK",
                        "country": "US",
                        "email": "",
                        "postcode": "12345",
                        "phone": "",
                        "address1": "blah blah",
                        "address2": ""
                    }
                },
                originalTimestamp: Date.now()
            }
        ])

        console.log('batch send ', data3)

    } catch (exception) {
        console.log('exception ', exception)
    }
}

ss()
